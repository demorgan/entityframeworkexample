﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkExample.Domain
{
    public class Item: Entity<int>
    {
        public string Name { get; set; }

        public ICollection<OrderItems> Orders { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}

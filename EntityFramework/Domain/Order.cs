﻿using System.Collections.Generic;

namespace EntityFrameworkExample.Domain
{
    public class Order: Entity<int>
    {
        public Order()
        {
            Items = new List<OrderItems>();
        }

        public int Number { get; set; }

        public ICollection<OrderItems> Items { get; set; }
    }
}

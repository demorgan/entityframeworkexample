﻿namespace EntityFrameworkExample.Domain.TablePerHierarchyDomain
{
    public class Phone: Entity<int>
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public int Price { get; set; }
    }
}

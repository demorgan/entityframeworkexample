﻿namespace EntityFrameworkExample.Domain.TablePerHierarchyDomain
{
    public class Smartphone: Phone
    {
        public string OS { get; set; }
    }
}

﻿namespace EntityFrameworkExample.Domain
{
    public class OrderItems: Entity<int>
    {
        public Order Order { get; set; }

        public Item Item { get; set; }

        public int Quantity { get; set; }

        public int OrderId { get; set; }

        public int ItemId { get; set; }
    }
}

﻿namespace EntityFrameworkExample.Domain
{
    interface IEntity<TId>
    {
        TId Id { get; set; }
    }
}

﻿using System.Data.Entity;
using EntityFrameworkExample.Domain;
using EntityFrameworkExample.Domain.TablePerHierarchyDomain;

namespace EntityFrameworkExample
{
    public class OnlineShopContext: DbContext
    {
        static OnlineShopContext()
        {
            Database.SetInitializer<OnlineShopContext>(null);
        }

        public OnlineShopContext() : base("OnlineShopConnection")
        {

        }

        public DbSet<Item> Items { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItems> OrderItems { get; set; }

        public DbSet<Phone> Phones { get; set; }
        public DbSet<Smartphone> Smarts { get; set; }
    }


   
}

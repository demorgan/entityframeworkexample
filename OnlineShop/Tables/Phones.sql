﻿CREATE TABLE [dbo].[Phones]
(
	[Id] INT NOT NULL  IDENTITY(1,1), 
    [Name] NVARCHAR(50) NOT NULL, 
    [Company] NVARCHAR(50) NOT NULL, 
    [Price] INT NOT NULL, 
	[OS] NVARCHAR(50) NULL,
    [Discriminator] NVARCHAR(128) NOT NULL,

	CONSTRAINT [PK_Phones] PRIMARY KEY CLUSTERED ([Id] ASC),
    
)

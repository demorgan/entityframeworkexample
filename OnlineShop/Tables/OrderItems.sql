﻿CREATE TABLE [dbo].[OrderItems]
(
	[Id] INT NOT NULL IDENTITY(1,1),
	[OrderId] INT NOT NULL,
	[ItemId] INT NOT NULL,
	[Quantity] INT NOT NULL DEFAULT 0, 

    CONSTRAINT [PK_OrdersItems] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_OrdersItems_Orders] FOREIGN KEY([OrderId]) REFERENCES [dbo].Orders ([Id]),
	CONSTRAINT [FK_OrdersItems_Items] FOREIGN KEY([ItemId]) REFERENCES [dbo].Items ([Id]),
)

﻿using EntityFrameworkExample;
using EntityFrameworkExample.Domain;

namespace EntityFrameworkTests.Factories
{
    public class OrderFactory
    {
        public static Order CreateOrder(OnlineShopContext context)
        {
            var order = new Order()
            {
                Number = 1000001
            };

            context.Orders.Add(order);
            context.SaveChanges();

            return order;
        }
    }
}

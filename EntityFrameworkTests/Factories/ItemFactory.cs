﻿using EntityFrameworkExample;
using EntityFrameworkExample.Domain;

namespace EntityFrameworkTests.Factories
{
    public class ItemFactory
    {
        public static Item CreateItem(OnlineShopContext context)
        {
            var item = new Item()
            {
                Name = "Orange"
            };

            context.Items.Add(item);
            context.SaveChanges();

            return item;
        }

        public static Item CreateItem(string name, OnlineShopContext context)
        {
            var item = new Item()
            {
                Name = name
            };

            context.Items.Add(item);
            context.SaveChanges();

            return item;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkExample;
using EntityFrameworkExample.Domain.TablePerHierarchyDomain;

namespace EntityFrameworkTests.Factories
{
    public class PhoneFactory
    {
        public static Phone CreatePhone(OnlineShopContext context)
        {
            var phone = new Phone()
            {
                Company = "Samsung",
                Name = "Samsung Galaxy S5",
                Price = 650
            };

            context.Phones.Add(phone);
            context.SaveChanges();

            return phone;
        }

        public static Smartphone CreateSmartphone(OnlineShopContext context)
        {
            var smartPhone = new Smartphone()
            {
                Name = "iPhone 6",
                Company = "Apple",
                Price = 32000,
                OS = "iOS"
            };

            context.Smarts.Add(smartPhone);
            context.SaveChanges();

            return smartPhone;
        }
    }
}

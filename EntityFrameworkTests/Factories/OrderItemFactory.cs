﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkExample;
using EntityFrameworkExample.Domain;

namespace EntityFrameworkTests.Factories
{
    public class OrderItemFactory
    {
        public static OrderItems CreateOrderItem(OnlineShopContext onlineShopContext, Order order, Item item)
        {
            var orderItem = new OrderItems()
            {
                Item = item,
                Order = order,
                Quantity = 5
            };

            onlineShopContext.OrderItems.Add(orderItem);
            onlineShopContext.SaveChanges();

            return orderItem;

        }

        public static OrderItems CreateOrderItem(OnlineShopContext onlineShopContext, Order order, Item item, int quantity)
        {
            var orderItem = new OrderItems()
            {
                Item = item,
                Order = order,
                Quantity = quantity
            };

            onlineShopContext.OrderItems.Add(orderItem);
            onlineShopContext.SaveChanges();

            return orderItem;

        }
    }
}

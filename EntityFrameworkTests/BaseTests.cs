﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using EntityFrameworkExample;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EntityFrameworkTests
{
    [TestClass]
    public class BaseTests
    {
        protected OnlineShopContext ShopContext;
        protected TransactionScope TransactionScope;

        [TestInitialize]
        public void TestSetup()
        {
            TransactionScope = new TransactionScope(TransactionScopeOption.RequiresNew);
            ShopContext = new OnlineShopContext();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            ShopContext.Dispose();
            TransactionScope.Dispose();
        }
    }
}

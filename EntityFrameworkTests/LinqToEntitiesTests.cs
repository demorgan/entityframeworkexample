﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkExample.Domain;
using EntityFrameworkTests.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EntityFrameworkTests
{
    [TestClass]
    public class LinqToEntitiesTests: BaseTests
    {
        [TestMethod]
        public void InnerJoin()
        {
            var createdOrder = OrderFactory.CreateOrder(ShopContext);
            var createdItem = ItemFactory.CreateItem(ShopContext);
            var createdOrderItem = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem);

            var result = from order in ShopContext.Orders
                join orderItem in ShopContext.OrderItems on order.Id equals orderItem.OrderId
                select new
                {
                    Number = order.Number,
                    Quantity = orderItem.Quantity
                };

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void InnerGroupJoin()
        {
            var createdOrder = OrderFactory.CreateOrder(ShopContext);
            var createdItem = ItemFactory.CreateItem(ShopContext);
            var createdOrderItem1 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem);
            var createdOrderItem2 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem);

            var query = from order in ShopContext.Orders
                join orderItem in ShopContext.OrderItems on order.Id equals orderItem.OrderId into orderItems
                select new
                {
                    Number = order.Number,
                    OrderItems = orderItems
                };

            var resultList = query.ToList();            

            Assert.AreEqual(1, resultList.Count);
            Assert.AreEqual(2, resultList.SelectMany(x => x.OrderItems).Count());
        }

        [TestMethod]
        public void LeftOuterJoin()
        {
            var createdOrder = OrderFactory.CreateOrder(ShopContext);

            var query = from order in ShopContext.Orders
                join orderItem in ShopContext.OrderItems on order equals orderItem.Order into groupJoin
                from subOrderItem in groupJoin.DefaultIfEmpty()
                select new
                {
                    Number = order.Number,
                    Quantity = subOrderItem.Quantity
                };

            Assert.AreEqual(1, query.Count());
        }

        [TestMethod]
        public void GroupByOperator()
        {
            var createdOrder = OrderFactory.CreateOrder(ShopContext);
            var createdItem = ItemFactory.CreateItem(ShopContext);
            var createdOrderItem1 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem);
            var createdOrderItem2 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem);

            var groups = from orderItem in ShopContext.OrderItems
                group orderItem by orderItem.ItemId;

            var queryResult = groups.ToList();

        }

        [TestMethod]
        public void GroupByOperator2()
        {
            var createdOrder = OrderFactory.CreateOrder(ShopContext);
            var createdItem = ItemFactory.CreateItem(ShopContext);
            var createdOrderItem1 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem, 5);
            var createdOrderItem2 = OrderItemFactory.CreateOrderItem(ShopContext, createdOrder, createdItem, 7);

            var groups = from orderItem in ShopContext.OrderItems
                group orderItem by orderItem.ItemId into groupped
                select new
                {
                    ItemId = groupped.Key,
                    QuantitySum = groupped.Sum(x => x.Quantity)
                };

            var groupResult = groups.ToList();

            Assert.AreEqual(12, groupResult.First().QuantitySum);
        }

        [TestMethod]
        public void IEnumerableTest1()
        {
            IEnumerable<Order> orders = ShopContext.Orders;
            var filteredOrders = orders.Where(x => x.Number == 90000).ToList();
        }

        [TestMethod]
        public void IEnumerableTest2()
        {
            //explicitly call IEnumerable version of Where
            ShopContext.Orders.Where((Func<Order, bool>)(x => x.Number == 9000)).ToList();
        }

        [TestMethod]
        public void IQueryableTest()
        {
            IQueryable<Order> orders = ShopContext.Orders;
            var filteredOrders = orders.Where(x => x.Number == 90000).ToList();
        }

        [TestMethod]
        public void AsNoTrackingTest()
        {
            ShopContext.Orders.AsNoTracking();//do not cache data and don't attach data to context
        }


    }
}

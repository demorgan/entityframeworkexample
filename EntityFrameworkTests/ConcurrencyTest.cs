﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkExample.Domain;
using EntityFrameworkTests.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EntityFrameworkTests
{
    [TestClass]
    public class ConcurrencyTest : BaseTests
    {
        [TestMethod]
        [ExpectedException(typeof(DbUpdateConcurrencyException))]
        public void EditOnConcurrency_ExpectingDbUpdateConcurrencyException()
        {
            var item = ItemFactory.CreateItem("Orange", ShopContext);

            var item2 = new Item()
            {
                Id = item.Id,
                Name = item.Name,
                RowVersion = item.RowVersion
            };

            item.Name = "Banana";
            ShopContext.SaveChanges();
            ShopContext.Entry(item).State = EntityState.Detached;

            ShopContext.Items.Attach(item2);
            item2.Name = "Apple";
            ShopContext.SaveChanges();

        }
    }
}

﻿using System.Data.Entity;
using System.Linq;
using System.Transactions;
using EntityFramework;
using EntityFrameworkExample;
using EntityFrameworkTests.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EntityFrameworkTests
{
    [TestClass]
    public class CrudTests: BaseTests
    {
       
        [TestMethod]
        public void CreateItem()
        {
            var item = ItemFactory.CreateItem(ShopContext);

            Assert.IsNotNull(item);
        }

        [TestMethod]
        public void EditItemName()
        {
            var item = ItemFactory.CreateItem("Orange", ShopContext);
            item.Name = "Banana";
            ShopContext.SaveChanges();

            Assert.AreEqual("Banana", ShopContext.Items.First(x => x.Id == item.Id).Name);
        }

        [TestMethod]
        public void EditItemNameInAnotherContextWithModifiyingState()
        {
            var item = ItemFactory.CreateItem("Orange", ShopContext);
            using (var shopContext = new OnlineShopContext())
            {
                item.Name = "Banana";
                shopContext.Entry(item).State = EntityState.Modified;
                shopContext.SaveChanges();
            }

            Assert.AreEqual("Banana", ShopContext.Items.First(x => x.Id == item.Id).Name);
        }


        [TestMethod]
        public void EditItemNameInAnotherContextWithAttachMethod()
        {
            var item = ItemFactory.CreateItem("Orange", ShopContext);
            using (var shopContext = new OnlineShopContext())
            {
                shopContext.Items.Attach(item);
                item.Name = "Banana";
                shopContext.SaveChanges();
            }

            Assert.AreEqual("Banana", ShopContext.Items.First(x => x.Id == item.Id).Name);
        }

        [TestMethod]
        public void DeleteItem()
        {
            var item = ItemFactory.CreateItem(ShopContext);
            
            ShopContext.Items.Remove(item);
            ShopContext.SaveChanges();

            Assert.IsNull(ShopContext.Items.FirstOrDefault(x => x.Id == item.Id));
        }

        [TestMethod]
        public void DeleteItemInAnotherContextWithModifiyingState()
        {
            var item = ItemFactory.CreateItem(ShopContext);

            using (var shopContext = new OnlineShopContext())
            {
                shopContext.Entry(item).State = EntityState.Deleted;
                shopContext.SaveChanges();
            }

            Assert.IsNull(ShopContext.Items.FirstOrDefault(x => x.Id == item.Id));
        }

        [TestMethod]
        public void DeleteItemInAnotherContextWithAttachMethod()
        {
            var item = ItemFactory.CreateItem(ShopContext);

            using (var shopContext = new OnlineShopContext())
            {
                shopContext.Items.Attach(item);
                shopContext.Items.Remove(item);
                shopContext.SaveChanges();
            }

            Assert.IsNull(ShopContext.Items.FirstOrDefault(x => x.Id == item.Id));
        }

      
    }
}
